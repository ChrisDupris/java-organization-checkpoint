package com.galvanize;

import com.galvanize.formatters.*;


public class Application
{
    public static void main(String[] args)
    {
        Formatter myFormatter = getFormatter(args[1]);
        Booking myBooking = Booking.parse(args[0]);
        System.out.print(myFormatter.format(myBooking));
    }
    public static Formatter getFormatter(String format)
    {
        Formatter formatter = null;
        if(format.equals("html"))
        {
            formatter = new HTMLFormatter();
        }
        if(format.equals("json"))
        {
            formatter = new JSONFormatter();
        }
        if(format.equals("csv"))
        {
            formatter = new CSVFormatter();
        }

        return formatter;
    }

}

