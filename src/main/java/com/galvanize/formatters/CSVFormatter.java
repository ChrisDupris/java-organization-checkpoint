package com.galvanize.formatters;

import  com.galvanize.Booking;
public class CSVFormatter implements Formatter
{
    @Override
    public String format(Booking book)
    {
        String myFormat;
        myFormat = "type,room number,start time,end time\n" +
                ""+book.getRoomType().getTypeRoom()+","+book.getRoomNumber()+","+book.getStartTime()+","+book.getEndTime();
        return myFormat;
    }
}
