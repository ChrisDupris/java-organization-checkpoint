package com.galvanize.formatters;

import com.galvanize.Booking;

public class JSONFormatter implements Formatter
{
    @Override
    public String format(Booking book)
    {
        String myFormat;
        myFormat = "{\n" +
                "  \"type\": \""+book.getRoomType().getTypeRoom()+"\",\n" +
                "  \"roomNumber\": "+book.getRoomNumber()+",\n" +
                "  \"startTime\": \""+book.getStartTime()+"\",\n" +
                "  \"endTime\": \""+book.getEndTime()+"\"\n" +
                "}";
        return myFormat;
    }
}
