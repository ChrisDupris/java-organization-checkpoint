package com.galvanize.formatters;

import com.galvanize.Booking;
import java.util.SplittableRandom;

public interface Formatter
{
    public String format(Booking book);
}
